import requests
import json
from bs4 import BeautifulSoup
import pandas
from collections import OrderedDict
from multiprocessing import Process
import sys


def fetchData(finalWebScrappingUrl, last_page):
    lst_cumulative_data = []
    try:
        for page in range(1, last_page):
            # base_url = "https://www.makaan.com/mumbai-property/"\
            # ulwe-flats-for-sale-50412?page="
            base_url = finalWebScrappingUrl+"?page="
            url = base_url+str(page)
            scrapped_request = requests.get(url)
            scrapped_content = scrapped_request.content

            scrapped_soup = BeautifulSoup(scrapped_content, "html.parser")
            search_result_container = scrapped_soup.find_all(
                "div", {"class": "search-result-wrap"})
            if len(search_result_container) > 0:
                listing_result_container_per_page = \
                    search_result_container[0].\
                    find_all("li", {"class": "cardholder"})

                p = Process(target=printProgress, args=(page,))
                p.start()

                for listing in listing_result_container_per_page:
                    d = OrderedDict()
                    title_line = listing.find("div", {"class": "title-line"})
                    if title_line is not None:
                        first_anchor = title_line.find("a")
                        meta_data = first_anchor.find("meta")
                        title = meta_data.get("content")
                        listing_highlights = listing.find(
                            "table", {"class": "listing-highlights"})
                        tbody_highlight = listing_highlights.find("tbody")
                        price_number = tbody_highlight.find(
                            "span", {"class": "val"}).text
                        price_unit = tbody_highlight.find(
                            "span", {"class": "unit"}).text
                        sq_ft_rate = tbody_highlight.find("td",
                                                          {"class": "rate"})\
                            .text
                        property_status = tbody_highlight.find(
                            "td", {"class": "val"}).text

                        listing_keypoints = listing.find_all(
                            "li", {"class": "keypoint"})

                        details = ""

                        for keypoint in listing_keypoints:
                            details = details + "  "+keypoint.text

                        d["Title"] = title
                        d["Price"] = ("{}{}").format(price_number, price_unit)
                        d["Rate"] = "Rs "+sq_ft_rate
                        d["Status"] = property_status
                        d["Details"] = details

                        lst_cumulative_data.append(d)

                p.join()

        df = pandas.DataFrame(lst_cumulative_data)
        df.to_csv("properties.csv", index=False)
    except KeyboardInterrupt:
        return False


def printInitial():
    print("Fetching url...")


def printProgress(pageNum):
    # print("{}%".format(pageNum))
    percent = int((pageNum/last_page)*100)
    sys.stdout.write("{} {} complete".format(percent, "%"))
    sys.stdout.flush()
    restart_line()


def restart_line():
    sys.stdout.write('\r')
    sys.stdout.flush()


if __name__ == '__main__':
    user_locality = input("Enter the locality in Mumbai/Navi Mumbai"
                          "where you want to search for property\n")  # "Ulwe"
    user_city = "Mumbai"

    # Get the redirect url
    p = Process(target=printInitial, args=())
    p.start()
    custom_url = "https://www.makaan.com/columbus/app/v6/typeahead?"\
        "query={}&typeAheadType=&city=&usercity={}&rows=1&enhance=gp&"\
        "category=buy&view=buyer&sourceDomain=Makaan&format=json".format(
            user_locality, user_city)

    r = requests.get(custom_url)
    c = r.content

    d = c.decode('utf-8')
    clean_dict = json.loads(d)
    try:
        redirectUrl = clean_dict["data"][0]["redirectUrl"]

        # finalWebScrappingUrl = "https://www.makaan.com/mumbai-property/"\
        # "ulwe-flats-for-sale-50412?page=1"
        finalWebScrappingUrl = "https://www.makaan.com/"+redirectUrl
        scrapped_request = requests.get(finalWebScrappingUrl)
        scrapped_content = scrapped_request.content

        scrapped_soup = BeautifulSoup(scrapped_content, "html.parser")
        # print(scrapped_soup.prettify)

        search_result_container = scrapped_soup.find_all(
            "div", {"class": "search-result-wrap"})
        listing_result_container_per_page = search_result_container[0].\
            find_all("li", {"class": "cardholder"})

        footer_container = scrapped_soup.find_all(
            "div", {"class": "search-result-footer"})

        pagination_container = footer_container[0].find_all(
            "div", {"class": "pagination"})

        pagination_data = pagination_container[0].find_all("li")
        last_page = 0
        for pagination in pagination_data:
            anchor = pagination.find_all("a")
            if anchor:
                if (str(anchor[0].text)).isdigit():
                    last_page = (int)(anchor[0].text)
        # print("Number of records/listing per page = {}".
        #       format(len(listing_result_container_per_page)))
        # print("Last page={}".format(last_page))

        p.join()
        print("Url obtained. Processing data:")
        resType = fetchData(finalWebScrappingUrl, last_page)
        if resType is None:
            print("CSV file created successfully")
        else:
            pass
    except (IndexError, KeyError, KeyboardInterrupt):
        print("Sorry! but we don't have any records for this city")
