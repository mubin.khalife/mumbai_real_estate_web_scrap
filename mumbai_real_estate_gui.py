import requests
import json
from bs4 import BeautifulSoup
import pandas
from collections import OrderedDict
import tkinter
from tkinter import messagebox
from tkinter.ttk import Progressbar
import inspect
import os
import sys
import subprocess
from _thread import start_new_thread
from queue import Queue

completion_queue = Queue()


class RealEstateScrap:
    def __init__(self, root, user_city):
        self.window = root
        self.window.bind("<Control-q>", lambda _event: self.window.destroy())
        self.window.bind("<Control-Q>", lambda _event: self.window.destroy())

        self.frm = tkinter.Frame(self.window)
        self.frm.grid(row=0, column=0)

        self.lblTopMessage = tkinter.Label(self.frm,
                                           text=self.__str__(),
                                           justify="center", wraplength=300)
        self.lblTopMessage.grid(row=1, column=0, sticky="ew")

        self.txtEnter = tkinter.Entry(self.frm, font="Ariel 20")
        self.txtEnter.grid(row=4, column=0, sticky="ew", pady=40, padx=(0, 10))

        self.txtEnter.bind("<Return>", lambda _event: self.searchProperty())
        self.txtEnter.bind("<Control-KeyRelease-a>",
                           lambda event: self.highlightAll(event))
        self.txtEnter.bind("<Control-KeyRelease-A>",
                           lambda event: self.highlightAll(event))
        self.txtEnter.focus_set()

        self.btnSearch = tkinter.Button(self.frm, text="Search",
                                        command=lambda:
                                        self.searchProperty())
        self.btnSearch.grid(row=4, column=2, sticky="we")
        self.user_city = user_city
        self.progress = None
        self.percentLbl = tkinter.Label(self.frm)
        self.percentLbl.grid(row=5, column=0, sticky="we")
        self.Lab = tkinter.Label(self.frm)
        self.Lab.grid(row=7, column=0, sticky="we")

    def highlightAll(self, event):
        self.txtEnter.select_range(0, 'end')
        self.txtEnter.icursor('end')

    def fetchData(self, finalWebScrappingUrl, last_page):
        progress_var = tkinter.DoubleVar()
        self.progress = Progressbar(
            self.window, orient=tkinter.HORIZONTAL, length=100,
            variable=progress_var,  mode='determinate')
        self.progress.grid(row=6, column=0, sticky="ew")

        lst_cumulative_data = []
        # last_page = 3
        self.Lab.config(text="")
        self.frm.update_idletasks()

        try:
            for page in range(1, last_page):
                base_url = finalWebScrappingUrl+"?page="
                url = base_url+str(page)
                scrapped_request = requests.get(url)
                scrapped_content = scrapped_request.content

                scrapped_soup = BeautifulSoup(scrapped_content, "html.parser")
                search_result_container = scrapped_soup.find_all(
                    "div", {"class": "search-result-wrap"})
                if len(search_result_container) > 0:
                    listing_result_container_per_page = \
                        search_result_container[0].\
                        find_all("li", {"class": "cardholder"})

                    percent = int((page/last_page)*100)
                    # self.progress['value'] = percent
                    self.percentLbl.config(
                        text="{} {} complete".format(str(percent), "%"))
                    progress_var.set(percent)

                    for listing in listing_result_container_per_page:
                        d = OrderedDict()
                        title_line = listing.find(
                            "div", {"class": "title-line"})
                        if title_line is not None:
                            first_anchor = title_line.find("a")
                            meta_data = first_anchor.find("meta")
                            title = meta_data.get("content")
                            listing_highlights = listing.find(
                                "table", {"class": "listing-highlights"})
                            tbody_highlight = listing_highlights.find("tbody")
                            price_number = tbody_highlight.find(
                                "span", {"class": "val"}).text
                            price_unit = tbody_highlight.find(
                                "span", {"class": "unit"}).text
                            sq_ft_rate = tbody_highlight.find("td",
                                                              {"class":
                                                               "rate"})\
                                .text
                            property_status = tbody_highlight.find(
                                "td", {"class": "val"}).text

                            listing_keypoints = listing.find_all(
                                "li", {"class": "keypoint"})

                            details = ""

                            for keypoint in listing_keypoints:
                                details = details + "  "+keypoint.text

                            d["Title"] = title
                            d["Price"] = ("{}{}").format(
                                price_number, price_unit)
                            d["Rate"] = "Rs "+sq_ft_rate
                            d["Status"] = property_status
                            d["Details"] = details

                            lst_cumulative_data.append(d)

                    self.frm.update_idletasks()

            self.percentLbl.config(
                text="100 {} complete".format("%"))
            progress_var.set(100)
            completion_queue.put("Success")

            df = pandas.DataFrame(lst_cumulative_data)
            df.to_csv("properties.csv", index=False)
        except KeyboardInterrupt:
            completion_queue.put("Error")
            return False

    def searchProperty(self):
        if str(self.txtEnter.get()).strip() == "":
            pass
        else:
            start_new_thread(self.initData, (self.txtEnter.get(),))

    def __str__(self):
        return "Enter the locality in Mumbai/Navi Mumbai"
        "where you want to search for property\n"

    def initData(self, user_locality):
        self.Lab.config(text="Fetching url...")
        self.frm.update_idletasks()
        custom_url = "https://www.makaan.com/columbus/app/v6/typeahead?"\
            "query={}&typeAheadType=&city=&usercity={}&rows=1&enhance=gp&"\
            "category=buy&view=buyer&sourceDomain=Makaan&format=json".format(
                user_locality, self.user_city)

        r = requests.get(custom_url)
        c = r.content

        d = c.decode('utf-8')
        clean_dict = json.loads(d)
        try:
            redirectUrl = clean_dict["data"][0]["redirectUrl"]
            finalWebScrappingUrl = "https://www.makaan.com/"+redirectUrl
            scrapped_request = requests.get(finalWebScrappingUrl)
            scrapped_content = scrapped_request.content

            scrapped_soup = BeautifulSoup(scrapped_content, "html.parser")

            footer_container = scrapped_soup.find_all(
                "div", {"class": "search-result-footer"})

            pagination_container = footer_container[0].find_all(
                "div", {"class": "pagination"})

            pagination_data = pagination_container[0].find_all("li")
            last_page = 0
            for pagination in pagination_data:
                anchor = pagination.find_all("a")
                if anchor:
                    if (str(anchor[0].text)).isdigit():
                        last_page = (int)(anchor[0].text)

            self.Lab["text"] = "Url obtained. Processing data:"
            start_new_thread(self.fetchData, (finalWebScrappingUrl, last_page))
            # resType = self.fetchData(finalWebScrappingUrl, last_page)
            # thread1 = threading.Thread(self.fetchData(finalWebScrappingUrl,
            #                                           last_page))
            # thread1.start()
            # if resType is None:
            if str(completion_queue.get()).lower() == "success":
                optn_selected = messagebox.askyesno("Download success",
                                                    "Do you want to open"
                                                    " the file?")
                if optn_selected:
                    script_dir = (os.path.dirname(os.path.abspath(
                        inspect.getfile(inspect.currentframe()))))
                    file_name = "properties.csv"
                    abs_file_path = os.path.join(script_dir, file_name)
                    opener = "open" if sys.platform == "darwin" else "xdg-open"
                    subprocess.call([opener, abs_file_path])
                # messagebox.showinfo("Properties fetched",
                #                     "CSV file created successfully")
                # script_dir = os.path.dirname(__file__)
                # file_name = "properties.csv"
                # abs_file_path = os.path.join(script_dir, file_name)
                # subprocess.run(['open', abs_file_path], check=True)
            else:
                pass
        except (IndexError, KeyError, KeyboardInterrupt):
            self.Lab.config(text="")
            self.frm.update_idletasks()
            messagebox.showerror("Invalid City", "Sorry! but we don't "
                                 "have any records for this city")


if __name__ == '__main__':
    window = tkinter.Tk()
    window.geometry("500x500")
    # Don't allow resizing along x or y direction
    window.resizable(0, 0)
    window.title("Mumbai Real Estate Web Scrap")
    scrap = RealEstateScrap(window, "Mumbai")
    # scrap.initData()
    window.grid_columnconfigure(0, weight=1)
    window.mainloop()
